﻿using System;
using DragonsLair.Appl;
using DragonsLair.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    [TestClass]

    public class DragonsLairTests

    {

        TournamentRepo tournamentRepo;

        Tournament currentTournament;

        [TestInitialize]

        public void SetupForTest()

        {

            tournamentRepo = new TournamentRepo();

            currentTournament = tournamentRepo.GetTournament("Vinter Turnering");

        }

        [TestMethod]

        public void TournamentHasEvenNumberOfTeams()

        {

            int numberOfTeams = currentTournament.GetTeams().Count;

            Assert.AreEqual(0, numberOfTeams % 2);

        }

        [TestMethod]

        public void EqualNumberOfWinnersAndLosersPerRound()

        {

            int numberOfRounds = currentTournament.GetNumberOfRounds();

            for (int round = 0; round < numberOfRounds; round++)

            {

                Round currentRound = currentTournament.GetRound(round);

                int numberOfWinningTeams = currentRound.GetWinningTeams().Count;

                int numberOfLosingTeams = currentRound.GetLosingTeams().Count;

                Assert.AreEqual(numberOfWinningTeams, numberOfLosingTeams);

            }

        }

        [TestMethod]

        public void OneWinnerInLastRound()

        {

            // Verifies there is exactly one winner in last round

            int numberOfRounds = currentTournament.GetNumberOfRounds();

            Round currentRound = currentTournament.GetRound(numberOfRounds - 1);

            int numberOfWinningTeams = currentRound.GetWinningTeams().Count;

            Assert.AreEqual(1, numberOfWinningTeams);

        }

        [TestMethod]

        public void AllMatchesInPreviousRoundsFinished()

        {

            bool matchesFinished = true;

            int numberOfRounds = currentTournament.GetNumberOfRounds();

            for (int round = 0; round < numberOfRounds; round++)

            {

                Round currentRound = currentTournament.GetRound(round);

                if (currentRound.IsMatchesFinished() == false)

                    matchesFinished = false;

            }

            Assert.AreEqual(true, matchesFinished);

        }

    }
}
