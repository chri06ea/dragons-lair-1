﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DragonsLair.UI;

namespace DragonsLair
{
    class Program
    {
        static void Main()
        {
            Program myProgram = new Program();
            myProgram.Run();
        }

        void Run()
        {
            Menu menu = new Menu("Dragons Lair");

            menu.AddMenuItem("Præsenter turneringsstilling", CommandType.PresentTournamentScore);
            menu.AddMenuItem("Planlæg runde i turnering", CommandType.PlanRoundInTournament);

            menu.Activate(true);
        }
    }
}
