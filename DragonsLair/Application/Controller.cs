﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DragonsLair.Appl
{
    public class Controller
    {
        private TournamentRepo tournamentRepository = new TournamentRepo();



        public void ShowScore(string tournamentName)
        {
            var teamScoreDict = new Dictionary<string, int>();

            var tournament = tournamentRepository.GetTournament(tournamentName);
            if (tournament == null)
                return;
            
            for(var round = 0; round < tournament.GetNumberOfRounds(); round++)
                foreach (var winningTeam in tournament.GetRound(round).GetWinningTeams())
                {
                    if (!teamScoreDict.ContainsKey(winningTeam.Name))
                        teamScoreDict[winningTeam.Name] = 0;
                    teamScoreDict[winningTeam.Name]++;
                }

            Console.WriteLine("Turnering: " + tournament.Name);
            Console.WriteLine("Spillede runder " + tournament.GetNumberOfRounds());
            Console.WriteLine("Spillede kampe " + teamScoreDict.Sum(x => x.Value));

            foreach (var sortedTeamScoreEntry in teamScoreDict.OrderByDescending(x => x.Value))
                Console.WriteLine(sortedTeamScoreEntry.Key + " " + sortedTeamScoreEntry.Value);

            Console.ReadLine();
        }

        public void ScheduleNewRound(string tournamentName, bool printNewMatches = true)
        {
            var tournament = tournamentRepository.GetTournament(tournamentName);
            if (tournament == null)
                return;

            var newRoundTeams = tournament.GetRound(tournament.GetNumberOfRounds() - 1).GetWinningTeams();
            if (newRoundTeams.Count != 0 && newRoundTeams.Count % 2 != 0)
                newRoundTeams.RemoveAt(0);

            Console.WriteLine("Tournament: " + tournamentName);
            Console.WriteLine("Runde: " + tournament.GetNumberOfRounds());
            Console.WriteLine("(" + newRoundTeams.Count/2 + " kampe)");

            for (var teamIndex = 0; teamIndex < newRoundTeams.Count; teamIndex++)
            {
                Console.Write(newRoundTeams[teamIndex].Name);
                if (teamIndex % 2 != 0) Console.WriteLine();
                else Console.Write(" - ");
            }

            Console.ReadKey();
        }
    }
}
