﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DragonsLair.Appl;

namespace DragonsLair.UI
{
    public class Menu
    {
        private Controller controller = new Controller();

        private MenuItem[] menuItems = new MenuItem[10];
        private int itemCount = 0;
        private string helpText = "Vælg et menupunkt eller 0 for at afslutte";

        public string Title { get; }

        public Menu(string title)
        {
            Title = title;
        }

        public void AddMenuItem(string itemTitle, CommandType command)
        {
            MenuItem menuItem = new MenuItem(itemTitle, command);
            menuItems[itemCount++] = menuItem;
        }

        private void DisplayMenu()
        {
            Console.Clear();
            Console.WriteLine(Title);
            Console.WriteLine();

            for (int i = 0; i < itemCount; i++)
            {
                Console.WriteLine("  " + (i + 1) + ". " + menuItems[i].Title);
            }
            Console.WriteLine();
            Console.WriteLine("(" + helpText + ")");
        }
        private CommandType GetMenuChoice()
        {
            CommandType result = CommandType.Exit;

            bool notExit = true;

            while (notExit)
            {
                DisplayMenu();

                string sAnswer = Console.ReadLine();
                int answer = 0;

                if (int.TryParse(sAnswer, out answer))
                {
                    if (answer >= 0 && answer <= itemCount)
                    {
                        result = (answer == 0) ? CommandType.Exit : menuItems[answer - 1].Command;
                        notExit = false;
                    }
                    else
                    {
                        Console.WriteLine("Ugyldigt menuvalg, prøv igen");
                        Console.ReadKey();
                    }
                }
                else
                {
                    Console.WriteLine("Ugyldigt menuvalg, prøv igen");
                    Console.ReadKey();
                }
            }

            return result;
        }

        public CommandType Activate(bool stayInMenu = false)
        {
            CommandType choice = CommandType.Exit;
            string tournamentName;

            do
            {
                choice = GetMenuChoice();

                switch (choice)
                {
                    case CommandType.PresentTournamentScore:
                        Console.Clear();
                        Console.WriteLine("Præsentér turneringsstilling");
                        Console.WriteLine("============================");
                        Console.WriteLine();
                        Console.Write("Angiv navn på turnering: ");
                        tournamentName = Console.ReadLine();
                        //
                        controller.ShowScore(tournamentName);
                        break;
                    case CommandType.PlanRoundInTournament:
                        Console.Clear();
                        Console.WriteLine("Planlæg runde i turnering");
                        Console.WriteLine("=========================");
                        Console.WriteLine();
                        Console.Write("Angiv navn på turnering: ");
                        tournamentName = Console.ReadLine();
                        //
                        controller.ScheduleNewRound(tournamentName);
                        break;
                }
            }
            while (choice != CommandType.Exit && stayInMenu);

            return choice;
        }

    }
}
