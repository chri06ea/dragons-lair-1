﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DragonsLair.UI
{
    public class MenuItem
    {
        public string Title { get; }
        public CommandType Command { get; }

        public MenuItem(string itemTitle, CommandType command)
        {
            Title = itemTitle;
            Command = command;
        }
    }
}
