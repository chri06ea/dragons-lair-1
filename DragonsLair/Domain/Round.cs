﻿using System.Collections.Generic;

namespace DragonsLair.Domain
{
    public class Round
    {
        private List<Match> matches = new List<Match>();
        
        public void AddMatch(Match m)
        {
            matches.Add(m);
        }

        public Match GetMatch(string teamName1, string teamName2)
        {
            foreach(var match in matches)
            {
                if ((match.FirstOpponent.Name == teamName1 &&
                    match.SecondOpponent.Name == teamName2) ||
                    (match.FirstOpponent.Name == teamName2 &&
                     match.SecondOpponent.Name == teamName1))
                    return match;
            }
            return null;
        }

        public bool IsMatchesFinished()
        {
            foreach(var match in matches)
                if (match.Winner == null)
                    return false;
            return true;
        }

        public List<Team> GetWinningTeams()
        {
            var result = new List<Team>();
            foreach (var match in matches)
                if (match.Winner != null)
                    result.Add(match.Winner);
            return result;
        }

        public List<Team> GetLosingTeams()
        {
            var result = new List<Team>();
            foreach (var match in matches)
                if (match.Winner != null)
                    result.Add(match.Winner == match.FirstOpponent ? match.SecondOpponent : match.FirstOpponent); ;
            return result;
        }
    }
}
